[![pipeline status](https://gitlab.com/omame/oreilly-free-ebooks-parser/badges/master/pipeline.svg)](https://gitlab.com/omame/oreilly-free-ebooks-parser/commits/master)

This is a scheduled CI pipeline that parses the list of [free ebooks](www.oreilly.com/free/) on the O'Reilly website and presents the results, sorted and in a more readable way, as a project snippet.

Take a look at [today's free ebooks](https://gitlab.com/omame/oreilly-free-ebooks-parser/snippets/1693751)!
