package main

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/omame/oreilly-free-ebooks-parser/internal/config"
	"gitlab.com/omame/oreilly-free-ebooks-parser/internal/gitlab"
	"gitlab.com/omame/oreilly-free-ebooks-parser/internal/oreilly"
)

func main() {
	config := config.ParseArgs()

	logrus.SetLevel(logrus.InfoLevel)
	if config.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
	ebooks, err := oreilly.ScrapeWebsite()
	if err != nil {
		logrus.Fatalf("failed to scrape the O'Reilly website: %s", err)
	}

	gitlabClient := gitlab.NewClient(config)
	err = gitlabClient.PublishSnippet(config.SnippetID, ebooks)
	if err != nil {
		logrus.Fatalf("failed to publish snippet: %s", err)
	}
}
