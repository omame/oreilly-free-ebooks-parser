package config

import (
	"flag"
	"os"

	"github.com/sirupsen/logrus"
)

// Config is the configuration that can be used in this binary.
type Config struct {
	Debug         bool
	DryRun        bool
	GitlabBaseURL string
	GitlabToken   string
	SnippetID     int
	Version       bool
}

// ParseArgs parses the command line arguments
func ParseArgs() *Config {
	args := &Config{}

	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.BoolVar(&args.DryRun, "dry-run", false, "don't apply anything, only look at what would happen")
	flag.BoolVar(&args.Version, "version", false, "show version and exit")
	flag.IntVar(&args.SnippetID, "snippet-id", 0, "ID of the snippet to update")
	flag.Parse()

	args.GitlabToken = os.Getenv("GITLAB_TOKEN")
	args.GitlabBaseURL = os.Getenv("GITLAB_BASEURL")

	if args.SnippetID == 0 {
		logrus.Fatal("snippet ID must be specified")
	}

	if args.GitlabToken == "" {
		logrus.Fatal("GITLAB_TOKEN is a required environment variable")
	}

	if args.GitlabBaseURL == "" {
		baseURL := os.Getenv("CI_SERVER_URL")
		logrus.Debugf("gitlab base URL not specified. Falling back to %s\n", baseURL)
		args.GitlabBaseURL = baseURL
	}

	return args
}
