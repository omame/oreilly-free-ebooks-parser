package config_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/omame/oreilly-free-ebooks-parser/internal/config"
)

func TestParseArgs(t *testing.T) {
	tt := []struct {
		name   string
		config *config.Config
		err    string
	}{
		{
			"valid configuration",
			&config.Config{
				Debug:         false,
				DryRun:        false,
				GitlabBaseURL: "https://gitlab.mydoma.in",
				GitlabToken:   "I-AM-A-TOKEN",
				SnippetID:     123,
			},
			"",
		},
	}

	for _, tc := range tt {
		a := assert.New(t)
		os.Setenv("GITLAB_BASEURL", tc.config.GitlabBaseURL)
		os.Setenv("GITLAB_TOKEN", tc.config.GitlabToken)

		t.Run(tc.name, func(t *testing.T) {
			res := config.ParseArgs()
			a.Equal(res, tc.config)
		})
	}
}
