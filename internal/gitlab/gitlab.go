package gitlab

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/omame/oreilly-free-ebooks-parser/internal/config"
)

// GitLabClient is a GitLab API client with some project options
type GitLabClient struct {
	Client *gitlab.Client
	DryRun bool
}

// NewClient creates a new API client to a GitLab endpoint
func NewClient(args *config.Config) GitLabClient {
	client, err := gitlab.NewClient(args.GitlabToken, gitlab.WithBaseURL(args.GitlabBaseURL))
	if err != nil {
		logrus.Fatalf("failed to create gitlab client: %q", err)
	}
	return GitLabClient{
		Client: client,
		DryRun: args.DryRun,
	}
}

// PublishSnippet publishes a snippet
func (g GitLabClient) PublishSnippet(snippetID int, content string) error {
	today := time.Now().Format("02-01-2006")
	description := fmt.Sprintf("Last update: %s", today)
	snippetOpts := &gitlab.UpdateProjectSnippetOptions{
		Title:       gitlab.String("Free O'Reilly books"),
		Description: &description,
		Content:     gitlab.String(content),
	}
	if g.DryRun {
		logrus.Infof("skipping publishing the snippet: dry-run mode enabled")
		return nil
	} else {
		_, _, err := g.Client.Snippets.UpdateSnippet(snippetID, (*gitlab.UpdateSnippetOptions)(snippetOpts))
		return err
	}
}
