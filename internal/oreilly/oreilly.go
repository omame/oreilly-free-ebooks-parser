package oreilly

import (
	"fmt"
	"net/http"
	"sort"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
)

// Ebook stores the details about the free ebooks
type Ebook struct {
	Author    string
	AuthorURL string
	BookURL   string
	Title     string
}

const (
	oreillyURL = "http://www.oreilly.com/free/"
)

// ScrapeWebsite scrapes the O'Reilly website and returns a slice of free ebooks
func ScrapeWebsite() (string, error) {
	ebooks := make([]Ebook, 0, 300)
	res, e := http.Get(oreillyURL)
	if e != nil {
		return "", e
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return "", err
	}

	doc.Find("div[name=FreeEbooks] .item .item-info").Each(func(i int, s *goquery.Selection) {
		ebookInfo := s.Find(".item-title")
		title := strings.TrimSpace(ebookInfo.Text())
		bookURL, _ := ebookInfo.Attr("href")

		authorInfo := s.Find(".item-authors a")
		author := strings.TrimSpace(authorInfo.Text())
		authorURL, _ := authorInfo.Attr("href")
		ebooks = append(ebooks, Ebook{
			author,
			authorURL,
			bookURL,
			title,
		})
	})

	if len(ebooks) == 0 {
		return "", fmt.Errorf("something went wrong fetching ebooks as I couldn't fetch any")
	}
	logrus.Debugf("fetched %d free ebooks", len(ebooks))

	var content string
	sort.Slice(ebooks, func(i, j int) bool { return ebooks[i].Title < ebooks[j].Title })
	for _, ebook := range ebooks {
		content += fmt.Sprintf("1. [%s](%s)", ebook.Title, ebook.BookURL)
		if ebook.Author != "" {
			content += fmt.Sprintf(" by [%s](%s)", ebook.Author, ebook.AuthorURL)
		}
		content += "\n"
	}

	return content, nil
}
